### SUMMARY
This module allows you to trigger link's click action with JavaScript.

### INSTALLATION
Using composer:
```
composer require drupal/autoclick --sort-packages
```

## USAGE
You can enable this feature adding the following tag's attributes:

* data-autoclick-timer: The wait time in seconds.

### SPONSORS
- [Fundación UNICEF Comité Español](https://www.unicef.es)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](http://drupal.org/u/pcambra)
- Manuel Egío [(facine)](http://drupal.org/u/facine)
