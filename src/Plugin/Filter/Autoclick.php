<?php

namespace Drupal\autoclick\Plugin\Filter;

use Drupal\Component\Utility\Html as HtmlUtility;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Filter to attach the libraries for the autoclick library.
 *
 * @Filter(
 *   id = "autoclick",
 *   title = @Translation("Autoclick"),
 *   description = @Translation("Attach the needed libraries for the autoclick plugin."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class Autoclick extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    $dom = HtmlUtility::load($text);
    $xpath = new \DOMXPath($dom);
    if ($xpath->query('//*[@data-autoclick-timer]')->length) {
      $result->addAttachments([
        'library' => [
          'autoclick/autoclick',
        ],
      ]);
    }

    return $result;
  }

}
